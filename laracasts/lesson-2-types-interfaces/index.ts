const greeting: string = 'hello world!'

const year: number = 2023

function addTwoNumbersUsingFunction(a: number, b: number): number {
    return a + b
}

const addTwoNumbersUsingArrowFunction = (a: number, b: number): number => a + b

const getFruitById = (id: string): {id: string, name: string, color: string} => ({
    id,
    name: 'orange',
    color: 'orange',
})

interface User {
    id: string // we can end with ';' ',' or nothing ''
    name: string, 
    age: number;
    email: string
}

const getUserById = (id: string): User => ({
    id,
    name: 'Sarah Fraichit',
    age: 123,
    email: '',
})

const user = getUserById('zog')

const saveUser = (user: User) => console.log('saving user', {user})

saveUser(user)

// typescript is structure typed

interface Book {
    id: string
    name: string
    releasedAt?: Date // optional property
}

const getBookById = (id: string): Book => ({
    id,
    name: 'nice book',
})

const saveBook = (book: Book) => console.log('saving book', {book})

const book: Book = getBookById('book-1')
saveBook(user) // won't raise an error because Book and User share the same contract, meaning same structure

// saveUser(book) // this one will raise an error since a Book lacks some of the properties of a User
