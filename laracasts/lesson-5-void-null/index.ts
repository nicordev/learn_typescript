// void, null, undefined

const log = (something: unknown): void => console.log(something)

const result = log('zog')

console.log(result)

interface User {
    id: string;
    email: string;
    name: string|null;
    age?: number; // optional property, so it's of type undefined
}

const createUser = (email: string): User => ({
    id: 'some-' + Math.floor(Math.random() * 100),
    email,
    name: null,
    // age is undefined
})

const user: User = createUser('zog')

console.log({user})
