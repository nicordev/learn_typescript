// any and unknown

// 'any' bypass the type checker entirely, it tells typescript to not do any check, avoid using it.
// const log = (something: any): void => console.log(something.toUpperCase())

// unknown will avoid calling methods that we are not certain that they can be called
const log = (something: unknown): void => {
    console.log(something) // no error
    // console.log(something.toUpperCase()) // error: typescript will tell us that we don't know if 'toUpperCase' can be used here

    if (typeof something === 'string') {
        console.log(something.toUpperCase())
    } else {
        console.log(something)
    }

}

log('hello')
log({hello: 'world'})

interface Fruit {
    name: unknown,
    picker: unknown
}

const orange: Fruit = {
    name: 'orange',
    picker: {specie: 'bird'}
}

if (
    orange.picker
    && typeof orange.picker === 'object'
    && Object.hasOwnProperty.call(orange.picker, 'specie')
) {
    // console.log(orange.picker.specie) // typescript error even if we know it exists
    console.log(
        (orange.picker as {specie: unknown}).specie
    )

    const pickerName = (orange.picker as {specie: unknown}).specie
    if (typeof pickerName === 'string') {
        console.log(pickerName.toUpperCase())
    }
}

// const canvas = document.getElementById('my-canvas') as HTMLCanvasElement
