// arrays

const fruits: string[] = ['orange', 'banana']
const numbers: Array<number> = [1, 2, 3]
const fruitsOrNumbers: (string|number)[] = [1, 'apple', 3]

// tuples: fixed length arrays and fixed items types

const tuple: [number, string] = [123, 'zog']
// const tupleWrong: [number, string] = ['zog', 123]

const arrayOfArray: Array<Array<number>> = [[1,2,3]]
