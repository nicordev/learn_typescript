//
// type declaration
//

type FruitType = {
    name: string;
    ripe: boolean;
}

//
// interface declaration
//

interface FruitInterface {
    name: string;
    ripe: boolean;
}

//
// Union
//

type Suit = 'CLUBS' | 'DIAMONDS' | 'HEARTS' | 'SPADES'

const pickedCard: Suit = 'HEARTS'

interface Person {
    name: string
}

interface Credential {
    login: string
}

type Member = Person | Credential // only `type` can be used to perform a union

const member: Member = {
    name: 'sarah',
    login: 'croche',
    // age: 32, // Type '{ name: string; login: string; age: number; }' is not assignable to type 'Member'.
}


//
// Intersection
//

interface 