# Typescript lesson 1

## installation

```sh
yarn add --dev typescript
```

## compile

```sh
./node_modules/typescript/bin/tsc helloLaracasts.ts
```

## compile and execute

using [ts-node](https://github.com/TypeStrong/ts-node#installation)

```sh
npm install -g typescript
npm install -g ts-node
```

and

```sh
ts-node tsFileHere
```

using [deno](https://docs.deno.com/runtime/manual/getting_started/installation)

```sh
curl -fsSL https://deno.land/x/install/install.sh | sh
```
```

and

```sh
deno run tsFileHere
```
