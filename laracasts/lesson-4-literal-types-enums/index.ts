//
// literal type
//

const flipCoin = (): 'Head'|'Tail' => Math.random() < 0.5 ? 'Head' : 'Tail'

console.log(flipCoin())

type Coin = 'head' | 'tail'
const myCoin: Coin = 'head'
console.log(myCoin)

//
// enum type
//

enum Suit {
    HEARTS,
    SPADES = 'SPADES', // we can assign values
    DIAMONDS = 'it does not matter though',
    CLUBS = '',
}

console.log(Suit.HEARTS)

const pickCard = (card: Suit) => {
    if (Suit.CLUBS === card) return 'you picked a club'

    return 'you did not pick a club'
}

console.log(
    pickCard(Suit.HEARTS),
    // pickCard('SPADES'), // won't work
)
